FROM golang:1.10.2

WORKDIR /go/src/rabbittest
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

ENV ENVELOPE_RABBITMQ "amqp://guest:guest@172.17.0.1:32775/"

CMD ["rabbittest"]